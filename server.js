var express  = require("express"),
    app      = express(),
    bodyParser = require("body-parser"),
    methodOverride = require("method-override"),
    path = require('path'),
    mongoose = require('mongoose');
//27017

// Connection to DB
mongoose.connect('mongodb://localhost/pamba', function(err, res){
    if(err) throw err;
    console.log('Connected to Database');
});

// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Import Models and Controllers
var pModel = require('./models/producto')(app, mongoose);
var cModel = require('./models/compra')(app, mongoose);
var uModel = require('./models/usuario')(app, mongoose);
var fModel = require('./models/filtro')(app, mongoose);
var fCModel = require('./models/filtro-cantidad')(app, mongoose);

var pCtrl = require('./controllers/productos');
var fCtrl = require('./controllers/filtros');
var rCtrl = require('./controllers/recomendador');
var mCtrl = require('./controllers/main');


// API routes
var apiRouter = express.Router();

apiRouter.route('/usuario/:id')
    .get(mCtrl.getUser)
    .post(mCtrl.updateUser);

apiRouter.route('/productos')
    .get(pCtrl.findAllProductos)
    .post(pCtrl.addProducto);

apiRouter.route('/productos/:id')
    .get(pCtrl.findProductoById)
    .put(pCtrl.updateProducto)
    .delete(pCtrl.deleteProducto);

apiRouter.route('/filtros')
    .get(fCtrl.findAllFiltros);

apiRouter.route('/filtros/:id')
    .get(fCtrl.findFiltroById);

apiRouter.route('/filtros-cantidad')
    .get(fCtrl.findAllFiltrosCantidad)
    //.post(fCtrl.addFiltro);
apiRouter.route('/filtros-cantidad/:id')
    .get(fCtrl.findFiltroCantidadById);

apiRouter.route('/downloadProduct/:id')
    .get(pCtrl.downloadFile);

apiRouter.route('/productoExtended/:idUsuario')
    .get(pCtrl.getProductoExtended);

apiRouter.route('/recomendador/:idProducto/:idUsuario')
    .get(rCtrl.recomendador);

app.use('/api', apiRouter);

// Index Route
var router = express.Router();

router.get('*', function(req, res) {
   console.log("GET /");
   res.sendFile(path.join(__dirname, 'dist/index.html'));
});
app.use(router);

// Start server
app.listen(3000, function() {
    console.log("Node server running on http://localhost:3000");
});

