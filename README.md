# PAMBAMEAN
## Configuracion del entorno
SOLO LA PRIMERA VEZ
### Instalar nodejs
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -  
sudo apt install -y nodejs build-essential
### Instalar NPM
sudo apt install -y npm
### Instalar AngularCLI
sudo npm install -g @angular/cli@latest
### Instalar MongoDB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927  
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list  
sudo apt update  
sudo apt install -y mongodb-org  
sudo nano /etc/systemd/system/mongodb.service  

> Pegar el siguiente texto:  
>
> [Unit]  
> Description=High-performance, schema-free document-oriented database  
> After=network.target  
>   
> [Service]  
> User=mongodb  
> ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf  
>   
> [Install]  
> WantedBy=multi-user.target  

sudo systemctl start mongodb  
sudo systemctl enable mongodb  
## Rular el servidor
### NPM Install
En caso de que se haya modificado el packages.json o se haya instalado algun modulo nuevo, ejecutar:
npm install
### Rular el servidor
Para arrancar el servidor, ejecutar:
> node server.js  

Cada vez que se realice algun cambio en el back, reiniciar el servidor.

Cada vez que se haga un cambio en Angular y se quiera ver representado en el servidor, sin necesidad de reiniciarlo, ejecutar:
> ng build  

### En caso de fallo

Em caso de fallo al hacer el ng build puede ser provocado por la versión de node (necesitamos la versión 6 o superior. Ejecutar:

>npm cache clean -f  
>npm install -g n  
>n stable  

# MongoDB
## Exportar
> mongodump --db 'nombre-bd' --out 'carpeta-destino'  

## Importar
> mongorestore --db 'nombre-bd' --drop 'carpeta-contenedora-bson'  

## ssh maquina amazon
siempre desde la carpeta donde se encuentra el archivo: "PAMBA.pem"
> ssh -i PAMBA.pem ubuntu@pamba-machine

para abrir las 3 terminales correspondientes(linea comandos (0), editor(1), servidor(2)):
> tmux attach

// para cambiar de terminal (0-2)  
ctrl + b + (numero) 

## Como generar un nuevo componente en angular2
> ng generate component my-new-component

Luego editar el:
> @Component({
>    selector:'nombre producto',
    
    a tu gusto (nombre que vas a usar)
