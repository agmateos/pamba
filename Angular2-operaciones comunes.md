## Como generar un nuevo componente en angular2
> ng generate component my-new-component

## Como enrutar un nuevo componente (recarga la página)
1) Añadir en el archivo html  
    href="/nombreRuta"  
2) Añadir al array de objetos "appRoutes" del archivo app.module.ts la entrada / objeto:  
{ path: 'nombreRuta', component: my-new-component },

## Como enrutar un nuevo componente (sin recargar la página)

1) Importar Router al archivo:
    import { Router } from '@angular/router';
2) Añadir router al constructor:
      constructor(public router: Router) { }
3) Ejecutar funcion de enrutado en click o donde sea:
    navigate(view) {
        this.router.navigate(['/'+ view]);
    }
4) Añadir al array de objetos "appRoutes" del archivo app.module.ts la entrada / objeto:  
{ path: 'nombreRuta', component: my-new-component },

