//File: controllers/productos.js
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Producto = mongoose.model('Producto');
var Compra = mongoose.model('Compra');
var http = require('http'),
    fileSystem = require('fs'),
    path = require('path');

//GET - Return all products in the DB
exports.findAllProductos = function (req, res) {
    console.log('busca todos')
    var filtros = {}
    if (Object.keys(res.req.query).length > 0) {
        filtros.$and = [];
    }
    for (fKey in res.req.query) {
        filtro = res.req.query[fKey];
        if (!Array.isArray(filtro)) {
            // By stackoverflow
            if (/^[\],:{}\s]*$/.test(filtro.replace(/\\["\\\/bfnrtu]/g, '@').
                replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
                replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                //the json is ok
                var json = JSON.parse(filtro);
                var and = {};
                if (json.max != null || json.min != null) {
                    and = { $and: [] };
                }

                if (json.min != null) {
                    min = { $gte: json.min };
                    f = {};
                    f[fKey] = min;
                    and.$and.push(f);
                }
                if (json.max != null) {
                    max = { $lte: json.max };
                    f = {};
                    f[fKey] = max;
                    and.$and.push(f);
                }
                filtros.$and.push(and);
            } else {
                //the json is not ok
                var or = { $or: [] };
                f = {};
                f[fKey] = filtro;
                or['$or'].push(f);
                filtros.$and.push(or);
                delete or;

            }
        } else {
            //Filtro//
            var or = { $or: [] };
            for (i in filtro) {
                f = {};
                f[fKey] = filtro[i];
                or['$or'].push(f);
            }
            filtros.$and.push(or);
            delete or;
        }
    }

    Producto.find(filtros, function (err, productos) {
        if (err) res.send(500, err.message);
        console.log(productos);
        console.log('GET /productos');
        res.status(200).jsonp(productos);
    });
};

//GET - Return a Product with specified ID
exports.findProductoById = function (req, res) {
    Producto.findById(req.params.id, function (err, producto) {
        if (err) return res.send(500, err.message);

        console.log('GET /producto/' + req.params.id);
        res.status(200).jsonp(producto);
    });
};

//POST - Insert a new Product in the DB
exports.addProducto = function (req, res) {
    console.log('POST');
    console.log(req.body);

    var producto = new Producto({
        title: req.body.title,
        price: req.body.price,
        provider: req.body.provider,
        rating: req.body.rating,
        img: req.body.img,
        size: req.body.size,
        downloads: req.body.downloads,
        extension: req.body.extension,
        tags: req.body.tags
    });

    producto.save(function (err, producto) {
        if (err) return res.status(500).send(err.message);
        res.status(200).jsonp(producto);
    });
};

//PUT - Update a register already exists
exports.updateProducto = function (req, res) {
    Producto.findById(req.params.id, function (err, producto) {
        if (err) return res.status(500).send(err.message);
        if ("undefined" == typeof producto) return res.status(404).send("No existe ese id");

        producto.title = req.body.title;
        producto.price = req.body.price;
        producto.provider = req.body.provider;
        producto.rating = req.body.rating;
        producto.img = req.body.img;
        producto.size = req.body.size;
        producto.downloads = req.body.downloads;
        producto.extension = req.body.extension;
        producto.tags = req.body.tags;

        producto.save(function (err) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(producto);
        });
    });
};

//DELETE - Delete a Producto with specified ID
exports.deleteProducto = function (req, res) {
    Producto.findById(req.params.id, function (err, producto) {
        producto.remove(function (err) {
            if (err) return res.status(500).send(err.message);
            res.status(200).send();
        })
    });
};

//GET - retrieves the file with specified ID
exports.downloadFile = function (req, res) {
    console.log('IniciandoDescarga');
    Producto.findById(req.params.id, function (err, producto) {
        // var ruta = producto.productFile;
        // res.status(200).jsonp(ruta);
        //res.status(200).jsonp(producto);
        //var filePath = path.join(__dirname, 'myfile.mp3');
        var ruta = producto.toObject().ruta;
        var stat = fileSystem.statSync(ruta);
        console.log(stat);

        res.writeHead(200, {
            'Content-Length': stat.size
        });
        //,
        //'Content-Type': 'audio/mpeg'
        //

        var readStream = fileSystem.createReadStream(ruta);
        // We replaced all the event handlers with a simple call to readStream.pipe()
        readStream.pipe(res);
    });

}

//GET - Return all purchasedProducts for a user id in the DB
exports.getProductoExtended = function (req, res) {
    Compra.find({ usuario: new ObjectId(req.params.idUsuario) })
        // .populate('idproducto')
        .exec(function (err, compras) {
            if (err) res.send(500, err.message);
            console.log('GET /Allproductos');
            Producto.populate(compras, {path: "producto"}, function(err, producto){
                res.status(200).jsonp(compras);
            })
        });
};