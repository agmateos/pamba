let item1 = {    
  pablo:2,
  luis:3,
  alicia:6
}

let item2 = {
  pablo:5,
  luis:7,
  alicia:8
}

let valoracionUsuario = 4;




//File: controllers/productos.js
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var Producto = mongoose.model('Producto');
var Compra = mongoose.model('Compra');
var http = require('http'),
    fileSystem = require('fs'),
    path = require('path');


exports.recomendador =  function (req, res) {
  console.log('*********INICIO RECOMENDADOR **********')

  //console.log(req.params)
  let resultado = 8;
    Producto.findById(req.params.idProducto, async function (err, producto) {
      if (err) return res.send(500, err.message);
      let queryKey = 'ratings.'+req.params.idUsuario;

      await Producto.find({ 'ratings.testUser':{$exists: true} })
      .exec( function (err, productos) {
        console.log('SEGUNDA QUERYYYYYYYYYYYY2', producto)



        if (err) res.send(500, err.message);
        
        //console.log('Productos encontrados2:',productos)


        resultado = recomendadorPorUsuarios(productos,producto,req.params.idUsuario)
        res.status(200).jsonp(resultado);

      console.log('resultado ', resultado)

          /*Producto.populate(compras, {path: "producto"}, function(err, producto){
              res.status(200).jsonp(compras);
          })*/
      });

      console.log('resultado2 ', resultado)

    });

  //res.status(200).jsonp({});
}

//Inicio del recomendador

// Recomendación basado en usuarios


function recomendadorPorUsuarios(productos, productoAValorar, idUsuario){
  console.log('')
  console.log('')

  console.log('*********INICIO RECOMENDADOR POR USUARIOS**********')

  let itemAValorar = productoAValorar.ratings
  let numeradorMediaPonderada = 0;
  let denominadorMediaPonderada = 0;

  for(producto of productos){

    let itemAprendizaje = producto.ratings;


    let prediccion = algoritmoSlopeOne(itemAValorar, itemAprendizaje, itemAprendizaje[idUsuario])
    if(prediccion.peso == 0)continue;
    numeradorMediaPonderada += prediccion.valorPredicho * prediccion.peso
    denominadorMediaPonderada += prediccion.peso

    console.log(prediccion)
  }
console.log('VALORACIÓN FINAL RECOMENDADA', numeradorMediaPonderada / denominadorMediaPonderada);
console.log('')
console.log('')

return numeradorMediaPonderada / denominadorMediaPonderada;
}



function algoritmoSlopeOne(valoracionesPObjetivo, valoracionesP2, valoracionUsuario){
  console.log('************ENTRA SLOPE ONE***********')

    let size = 0

    var numerador = 0 
    //console.log(valoracionesPObjetivo);
    for (let usuario in valoracionesPObjetivo){
      let valoracionP1 = valoracionesPObjetivo[usuario];
      
  
      
      if(!valoracionesP2[usuario])continue;

      let valorarcionP2 = valoracionesP2[usuario];
      //console.log ('numerador1', numerador)
      numerador += valorarcionP2-valoracionP1;
      size += 1;
    };

    let diferenciaMedia = numerador/size;
    //console.log(valoracionUsuario + diferenciaMedia)
    return {valorPredicho: valoracionUsuario + diferenciaMedia,
            peso: size
    }

}



algoritmoSlopeOne(item1, item2, valoracionUsuario);