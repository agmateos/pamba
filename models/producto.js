exports = module.exports = function(app, mongoose){
    var productoSchema = new mongoose.Schema({
        title: { type: String },
        price: { type: Number },
        provider: { type: Number },
        rating: { type: Number },
        img: { type: String },
        size: { type: Number },
        ratings: { type: Object },
        categories: { type: [String] },
        downloads: { type: Number },
        extension: { type: String },
        tags: { type: [String] }, 
        ruta: {type: String},      
    });
/*
size in MB
Campos ID (String)
 y 
version (Number?)
se generan por defecto
*/
    mongoose.model('Producto', productoSchema);
};
