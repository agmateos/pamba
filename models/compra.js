exports = module.exports = function(app, mongoose){
    var compraSchema = new mongoose.Schema({
        oroducto: { type: mongoose.Schema.Types.ObjectId, ref: 'Producto' },
        usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario' },
        precio: { type: Number },
        valoracion: { type: Number },
        fecha: { type: Date, default: Date.now},
        estado: { type: String },  
    });
/*
size in MB
Campos ID (String)
 y 
version (Number?)
se generan por defecto
*/
    mongoose.model('Compra', compraSchema);
};
