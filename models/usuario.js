exports = module.exports = function(app, mongoose){
    var usuarioSchema = new mongoose.Schema({
        user_name: { type: String },
        user_surname: { type: String },
        nickename: { type: String },
        email: { type: String },
        field_tags: { type: [String]},
        region: { type: String },  
        account_type: { type: String },       
        categories: { type: [String]},

    });

    mongoose.model('Usuario', usuarioSchema);
};
