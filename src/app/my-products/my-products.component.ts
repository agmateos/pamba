import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../productos.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-products',
  templateUrl: './my-products.component.html',
  styleUrls: ['./my-products.component.css']
})
export class MyProductsComponent implements OnInit {

  private productos = [];

  constructor(private pService: ProductosService, public auth: AuthService, public router: Router) { }

  openProduct(id) {
    this.router.navigate(['/product/' + id]);
  }

  ngOnInit() {
    this.getProductoExtended()
  }

  getProductoExtended() {
    this.pService.getProductoExtended(this.auth.getUsuarioOLD()['id']).subscribe(res => this.productos = res.json());
  }
}
