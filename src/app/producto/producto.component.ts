import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  @Input() p: any;

  private img="";
  private title = 'Name-test';
  private price = 0;
  private provider = "provider-test";
  private rating = 0;

  init() {
    this.img = this.p.img;
    this.title = this.p.title;
    this.price = this.p.price;
    this.provider = this.p.provider;
    this.rating = this.p.rating;
  }

  constructor() {
    // console.log(this.p);
  }

  ngOnInit(){
    console.log("producto", this.p);
    this.init();
  }

}
