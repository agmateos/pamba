import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { saveAs as importedSaveAs } from "file-saver";
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {

  constructor(private aService: AuthService, public router: Router, public route: ActivatedRoute) { }

  public user;
  public categories = ''
  ngOnInit() {
    /*
    this.route.params // (+) converts string 'id' to a number 
    .switchMap((params: Params) => this.aService.getUsuario(params['id'])).subscribe(res => {
      console.log(res.json())
    });
    */

   this.route.params // (+) converts string 'id' to a number 
   .switchMap((params: Params) => this.aService.getUsuario('000000000000000000000001')).subscribe(res => {
      console.log(res.json())
      this.user = res.json()
      this.categories =this.user.categories.join(", ");
      console.log(this.categories)
      /*for(let categorie of this.user.categories){
        this.categories += categorie
      }*/
    });
  }

  saveUser(){
    console.log(this.categories.split(', '));
    this.user.categories = this.categories.split(', ');
    this.aService.updateUsuario(this.user);
    console.log(this.user);
  }

}
