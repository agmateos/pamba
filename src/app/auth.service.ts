import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import auth0 from 'auth0-js';
import { Http, Response, RequestOptions, ResponseContentType } from '@angular/http';
import { catchError, retry } from 'rxjs/operators';


@Injectable()
export class AuthService {

  private userURL = 'api/usuario';


  auth0 = new auth0.WebAuth({
    clientID: '2JvZuThzTvquvBLfLpSzMd7A2VYOvXMc',
    domain: 'pamba.eu.auth0.com',
    responseType: 'token id_token',
    audience: 'https://pamba.eu.auth0.com/userinfo',
    redirectUri: 'http://52.29.128.111:3000/callback',
    scope: 'openid'
  });

  constructor(public router: Router, private http: Http) { }

  public login(): void {
    this.auth0.authorize();
  }

  public handleAuthentication(): void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.router.navigate(['/home']);
      } else if (err) {
        this.router.navigate(['/home']);
        console.log(err);
      }
    });
  }

  private setSession(authResult): void {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    // Go back to the home route
    this.router.navigate(['/']);
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  public getUsuario(idUsuario) {
    console.log('idUsuario',idUsuario)
    return this.http.get(this.userURL + '/' + idUsuario);
    //return { id: '000000000000000000000001' };
  }

  public updateUsuario(usuario) {
    console.log('idUsuario',usuario._id)
    console.log(usuario);
    return this.http.post(this.userURL + '/' + usuario._id, usuario).subscribe(result => console.log(result))
    //return { id: '000000000000000000000001' };
  }
  public handleError(err){
    console.log(err);
  }

  public getUsuarioOLD(): Object {
    return { id: '000000000000000000000001' };
  }
}