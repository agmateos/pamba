import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class ProductosService {
  private productosURL = 'api/productos';
  private recomendadorURL = 'api/recomendador';
  private productoExtendedURL = 'api/productoExtended';
  private descargaURL = 'api/downloadProduct';
  private filtros = {};

  constructor(private http: Http) { }

  /*destildear(str) {
    var tittles = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
    var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
    for (var i = 0; i < tittles.length; i++) {
      str = str.replace(tittles.charAt(i), original.charAt(i)).toLowerCase();
    };
    return str;
  }*/
  getProductos() {
    console.log('productos');
    return this.http.get(this.productosURL, {
      params: this.filtros
    });
    //.map(this.extractData)
    //.catch(this.handleError);
  }

  getProducto(id: number) {
    var params: URLSearchParams = new URLSearchParams();
    for (let key in this.filtros) {
      params.set(key, this.filtros[key]);
    }

    return this.http.get(this.productosURL + '/' + id, {
      search: params
    });
    //.map(this.extractData)
    //.catch(this.handleError);
  }

  getRecomendacion(idProducto: number, idUsuario: string) {
     return this.http.get(this.recomendadorURL + '/' + idProducto + '/' + idUsuario);
    //.map(this.extractData)
    //.catch(this.handleError);
  }

  getProductoExtended(idUsuario: number) {
    return this.http.get(this.productoExtendedURL + '/' + idUsuario, {
    });
  }

  downloadFile(id): Observable<Blob> {
    let options = new RequestOptions({ responseType: ResponseContentType.Blob });
    return this.http.get(this.descargaURL + '/' + id, options)
      .map(res => res.blob())
      .catch(this.handleError)
  }

  /*getProductoURL(id: number){
    return

  }*/

  setFiltros(key, filtros) {
    this.filtros[key] = filtros;
    console.log("filtros:", this.filtros);
    return this.getProductos();
  }

  private extractData(res: Response) {
    let body = res.json();
    console.log(body);
    return body.data || [];
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }


}
