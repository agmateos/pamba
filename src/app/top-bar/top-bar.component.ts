import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit {
  private title = "Pamba store";

  constructor(public auth: AuthService) {
    auth.handleAuthentication();
  }
  ngOnInit() {
  }

}
