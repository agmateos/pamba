import { Component, OnInit, Input } from '@angular/core';
import { ProductosService } from '../productos.service';
import { PasoParametrosService } from '../paso-parametros.service';


declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'filtro-cantidad',
  templateUrl: './filtro-cantidad.component.html',
  styleUrls: ['./filtro-cantidad.component.css']
})
export class FiltroCantidadComponent implements OnInit {
  @Input() fc: any;

  private title = 'Name-test';
  private name = 'Name-test';
  private units: String[];

  public min: Number;
  public max: Number;
  init() {
    this.title = this.fc.title;
    this.name = this.fc.name;
    this.units = this.fc.filtros;
  }

  constructor(private pService: ProductosService, private ppService: PasoParametrosService) { }

  public checkSelected() {
    let min = parseFloat(jQuery('#' + this.name + "_low").val());
    let max = parseFloat(jQuery('#' + this.name + "_high").val());
    if (min != null) {
      this.min = min;
    } else {
      this.min = -1;
    } if (max != null) {
      this.max = max;
    } else {
      this.max = -1;
    }
    this.pService.setFiltros(this.name, { min: this.min, max: this.max }).subscribe(res => { this.ppService.setProductos(res.json()) });
  }

  ngOnInit() {
    this.init();
  }
}
