import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroCantidadComponent } from './filtro-cantidad.component';

describe('FiltroCantidadComponent', () => {
  let component: FiltroCantidadComponent;
  let fixture: ComponentFixture<FiltroCantidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroCantidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroCantidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
