import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable()
export class FiltrosService {
    private filtrosURL = 'api/filtros';
    private filtrosCantidadURL = 'api/filtros-cantidad';


    constructor(private http: Http) { }

    getFiltros() {
        return this.http.get(this.filtrosURL);
    }

    getFiltro(id: number) {
        return this.http.get(this.filtrosURL + '/' + id);
    }

    getFiltrosCantidad() {
        return this.http.get(this.filtrosCantidadURL);
    }

    getFiltroCantidad(id: number) {
        return this.http.get(this.filtrosCantidadURL + '/' + id);
    }
}
