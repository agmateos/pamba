import { Injectable } from '@angular/core';
import { Producto } from './app.component';


@Injectable()
export class PasoParametrosService {
  //private productos = {}
  private aProductos : Producto[];
  private aCategorias= []


  constructor() { }

  getProductos() {
    console.log(this.aProductos)
    return this.aProductos;
  }

  setProductos(productos) {
    this.aProductos = productos
    this.orderProductos();
  }

//producto.categories
  orderProductos(){
    let size = this.aCategorias.length
    let newAProductos = [];
    console.log(this.aCategorias)
    while(size>=0){
      for(let pIndex in this.aProductos){
        let producto = this.aProductos[pIndex];
        let arrIntersection = this.aCategorias.filter(value => -1 !== producto.categories.indexOf(value)) //hayamos intersección de los dos arrays
        if(arrIntersection.length == size){
          newAProductos.push(producto);
        }      
      }
    size--;
    }
    this.aProductos = newAProductos;
  }

      /*
      let categoriasCoincidentes = 0
      for(let categorie of producto.categories){
        if(producto.categories){
          categoriasCoincidentes++;
        }
      }*/


  getCategorias() {
    console.log(this.aCategorias)
    return this.aCategorias;
  }

  setCategorias(categorias) {
    this.aCategorias = categorias
    this.orderProductos();
  }
}
