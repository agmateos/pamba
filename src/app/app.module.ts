import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RatingModule } from "ngx-rating";
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MainDisplayComponent } from './main-display/main-display.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { LeftBarComponent } from './left-bar/left-bar.component';
import { FiltrosBusquedaComponent } from './filtros-busqueda/filtros-busqueda.component';

import { ProductosService } from './productos.service';
import { PasoParametrosService } from './paso-parametros.service';
import { FiltrosService } from './filtros.service';
import { AuthService } from './auth.service';
import { ProductoComponent } from './producto/producto.component';
import { FiltroComponent } from './filtro/filtro.component';
import { FiltroCantidadComponent } from './filtro-cantidad/filtro-cantidad.component';
import { CallbackComponent } from './callback/callback.component';
import { ProductDetailedComponent } from './product-detailed/product-detailed.component';

import { RouterModule, Routes } from '@angular/router';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { MessagesComponent } from './messages/messages.component';
import { MyProductsComponent } from './my-products/my-products.component';
import { ProductRowComponent } from './product-row/product-row.component';
import { MessagesFilterBarComponent } from './messages-filter-bar/messages-filter-bar.component';

const appRoutes: Routes = [
  { path: 'home', component: MainDisplayComponent },
  { path: 'profile', component: ProfileViewComponent },
  { path: 'messages', component: MessagesComponent },
  { path: 'myProducts', component: MyProductsComponent },  
  { path: 'callback', component: CallbackComponent },
  { path: 'product/:id', component: ProductDetailedComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];


@NgModule({
  declarations: [
    AppComponent,
    MainDisplayComponent,
    TopBarComponent,
    LeftBarComponent,
    FiltrosBusquedaComponent,
    ProductoComponent,
    FiltroComponent,
    FiltroCantidadComponent,
    CallbackComponent,
    ProductDetailedComponent,
    ProfileViewComponent,
    MessagesComponent,
    MyProductsComponent,
    ProductRowComponent,
    MessagesFilterBarComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpModule,
    RatingModule,
    FormsModule
  ],
  providers: [ProductosService, FiltrosService, AuthService, PasoParametrosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
