import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}

export class Producto {
  title: string;
  price: number;
  provider: number;
  rating: number;
  img: string;
  size: Number;
  downloads: Number;
  extension: String;
  tags: String[];
  //productFile: String;
  _id: String;
  _v: String;
  categories: String[];
  strCategories: String;

  constructor(){
    this.title = "";
    this.price = 0;
    this.provider = 0;
    this.rating = 0;
    this.img = "";
    this.size = 0;
    this.downloads = 0;
    this.extension = "";
    this.tags = [];
    //this.productFile = ""
    this._id = "";
    this._v = "";
    this.categories = [];
  }
}

export class Filtro {
  title: String;
  name: String;
  filtros: String[];
  type:String;
  _id: String;
  _v: String;
}

export class FiltroCantidad {
  title: String;
  name: String;
  minValue: Number;
  maxValue: Number;
  _id: String;
  _v: String;
}
