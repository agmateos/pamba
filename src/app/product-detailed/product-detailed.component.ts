import { Component, OnInit } from '@angular/core';
import { ProductosService } from '../productos.service';
import { Producto } from '../app.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { saveAs as importedSaveAs } from "file-saver";
import 'rxjs/add/operator/switchMap';
var slugify = require('slugify');

@Component({
  selector: 'app-product-detailed',
  templateUrl: './product-detailed.component.html',
  styleUrls: ['./product-detailed.component.css']
})
export class ProductDetailedComponent implements OnInit {

  private producto: Producto = new Producto();
  private indiceAfinidad = 0;

  constructor(private pService: ProductosService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.route.params);
    console.log(2)
    let user = 'testUser'
    //this.pService.getRecomendacion(1111111111, 999999);
    this.route.params // (+) converts string 'id' to a number 
    .switchMap((params: Params) => this.pService.getRecomendacion(params['id'],user)).subscribe(res => {
      this.indiceAfinidad = Math.trunc(res.json()*10)
    });

    this.route.params // (+) converts string 'id' to a number 
      .switchMap((params: Params) => this.pService.getProducto(params['id'])).subscribe(res => {
        this.producto = res.json()
        this.producto.strCategories = this.producto.categories.join(", ");
      });
  }

  close() {
    this.router.navigate(['/home']);
  }

  buyProduct() {
    //var idProduct = this.producto._id;
    //this.pService.downloadFile(idProduct)
    this.pService.downloadFile(this.producto._id).subscribe(blob => {
      if (this.producto.extension.length > 0) {
        var extension = '.' + this.producto.extension;
      } else {
        var extension = "";
      }
      var nombre = slugify(this.producto.title) + extension
        //console.log(nombre);
      importedSaveAs(blob, nombre);
    })
  }
}
