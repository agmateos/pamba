import { Component, OnInit } from '@angular/core';
import { FiltrosService } from '../filtros.service';
import { Filtro } from '../app.component';
import { FiltroCantidad } from '../app.component';


import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'filtros-busqueda',
  templateUrl: './filtros-busqueda.component.html',
  styleUrls: ['./filtros-busqueda.component.css']
})
export class FiltrosBusquedaComponent implements OnInit {
  errorMessage: string;
  private aFiltros: Filtro[] = [];
  private aFiltrosCantidad: FiltroCantidad[] = [];

  constructor(private fService: FiltrosService) { }

  ngOnInit() {
    this.fService.getFiltros().subscribe(res => {
      // for (let filtro of res.json()) {
      //   if (filtro.type = "check") {
      //     this.aFiltros.push(filtro);
      //   }else{
      //     this.aFiltrosCantidad.push(filtro);        
      //   }
      // }
      this.aFiltros = res.json()
    });
    //this.fService.getFiltrosCantidad().subscribe(res => { this.aFiltrosCantidad = res.json() });
  }
}
